package jauhararifin.test.thrift;

import jauhararifin.test.thrift.arith.ArithService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.thrift.protocol.TBinaryProtocol;
import org.apache.thrift.protocol.TJSONProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.protocol.TSimpleJSONProtocol;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;

/**
 * @since 7/22/2017.
 */
public class Client {

  private static final Logger logger = LogManager.getLogger(Client.class);

  public static void main(String[] args) throws Exception {
    TTransport transport = new TSocket("localhost", 9090);
    transport.open();

    TProtocol protocol = new TJSONProtocol(transport);
    ArithService.Client client = new ArithService.Client(protocol);

    System.out.printf("1 + 2 = %d\n", client.plus(1, 2));
    System.out.printf("1 - 2 = %d\n", client.minus(1, 2));

    transport.close();
  }

}
