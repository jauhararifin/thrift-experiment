package jauhararifin.test.thrift;

import jauhararifin.test.thrift.arith.ArithService;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TJSONProtocol;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TSimpleServer;
import org.apache.thrift.transport.TServerSocket;
import org.apache.thrift.transport.TServerTransport;

import java.net.InetSocketAddress;

/**
 * @since 7/22/2017.
 */
public class Server {

  public static void main(String[] args) throws Exception {
    InetSocketAddress socketAddress = new InetSocketAddress("0.0.0.0", 9090);
    TServerTransport serverTransport = new TServerSocket(socketAddress);
    ArithService.Iface handler = new ArithService.Iface() {
      @Override
      public int plus(int a, int b) throws TException {
        System.out.printf("Plus %d %d\n", a, b);
        return a + b;
      }

      @Override
      public int minus(int a, int b) throws TException {
        System.out.printf("Minus %d %d\n", a, b);
        return a - b;
      }
    };
    ArithService.Processor processor = new ArithService.Processor<>(handler);
    TServer server = new TSimpleServer(new TServer.Args(serverTransport).processor(processor).protocolFactory(new TJSONProtocol.Factory()));
    server.serve();
  }

}
