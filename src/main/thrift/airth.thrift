namespace java jauhararifin.test.thrift.arith

service ArithService {
    i32 plus(1:i32 a, 2:i32 b),
    i32 minus(1:i32 a, 2:i32 b),
}